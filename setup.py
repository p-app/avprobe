from setuptools import setup, find_packages

setup(
    name='avprobe',
    version='0.1.0a1',
    packages=find_packages(),
    url='',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='admin@p-app.ru',
    description='avprobe wrapper',
    install_requires=[
        'textfsm==0.3.2',
        'python-dateutil'
    ],
    entry_points={
        'console_scripts': [
            'avprobe = avprobe.cli:run_cli'
        ]
    }
)
