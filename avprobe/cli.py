from argparse import ArgumentParser
from pprint import pprint

from .avprobe import AVProbe

__all__ = [
    'run_cli'
]


def run_cli():
    parser = ArgumentParser(description='AVProbe wrapper.')
    parser.add_argument('-f', '--filepath', required=True, nargs='+',
                        help='File to probe.')
    parser.add_argument('--command-filepath', required=False,
                        default='/usr/bin/avprobe', help='avprobe binary path.')

    args = parser.parse_args()

    avprobe = AVProbe(args.command_filepath)

    for filepath in args.filepath:
        data = avprobe.parse_file(filepath)
        pprint(data)


if __name__ == '__main__':
    run_cli()
