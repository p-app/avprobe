Value Filldown,Required input (\d)
Value Filldown,Required format ([\w,]+)
Value Filldown,Required filepath (.+)
Value Filldown title (.+)
Value Filldown encoder (.+)
Value Filldown creation_time ([\d\.:\-ZT]+)
Value Filldown,Required duration (\d+:\d+:\d+)
Value Filldown,Required bitrate (\d+\s*[\w\/]+)
Value Required stream (\d)
Value language (\w+)
Value Required type (Video|Audio|Subtitle)
Value Required details (.+)


Start
  ^Input #${input}, ${format}, from '${filepath}':
  ^\s{2}Metadata: -> Continue
  ^\s{4}title\s*: ${title} -> Continue
  ^\s{4}encoder\s*: ${encoder} -> Continue
  ^\s{4}creation_time\s*: ${creation_time} -> Continue
  ^\s{2}Duration: ${duration}(?:\.\d+), start: 0\.0+, bitrate: ${bitrate} -> Stream

Stream
  ^\s{4}Stream #${input}:${stream}(\(${language}\))?: ${type}: ${details} -> Record
