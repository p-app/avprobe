import time
from datetime import timedelta
from functools import partial
from itertools import groupby
from operator import itemgetter
from os.path import abspath
from re import compile
from subprocess import run, PIPE

from dateutil.parser import parse
from pkg_resources import resource_filename
from textfsm import TextFSM

__all__ = [
    'AVProbe'
]

_AVPROBE_OUTPUT_TEMPLATE = resource_filename(
    'avprobe', 'template/avprobe.txt'
)

_SUBTITLE_DETAILS_PATTERN = compile(
    r'^.+?(?P<default>\(default\))?\s*$'
)

_AUDIO_DETAILS_PATTERN = compile(
    r'^(?P<codec>\w+.*),\s+'
    r'(?P<frequency>\d+\s*[kK]?Hz),\s+'
    r'(?P<misc>\S+.+),\s+'
    r'(?P<bitrate>\d+\s*[kKmM]?[bB]/?[sS](ec)?)\s*'
    r'(?P<default>\(default\))?\s*$'
)

_VIDEO_DETAILS_PATTERN = compile(
    r'^(?P<codec>\w+.*),\s+'
    r'(?P<ratio>\d+x\d+(\s+\[[^\]]+\])?),\s+'
    r'(?:(?P<bitrate>\d+\s*[kKmM]?[bB]/?[sS](ec)?),\s+)?'
    r'(?:(?P<fps>\d+(?:\.\d+)?)\s+fps,\s+)?'
    r'(?P<misc>\S+.+?)\s*'
    r'(?P<default>\(default\))?\s*$'
)


def _group_stream(pattern, data):
    for stream, language, details in data:
        search = pattern.search(details)
        struct = {
            'stream': int(stream),
            'language': language or 'unknown',
            **search.groupdict()
        }
        struct['default'] = bool(struct['default'])
        yield struct


_STREAM_TYPE_GROUPER = {
    'Subtitle': partial(_group_stream, _SUBTITLE_DETAILS_PATTERN),
    'Audio': partial(_group_stream, _AUDIO_DETAILS_PATTERN),
    'Video': partial(_group_stream, _VIDEO_DETAILS_PATTERN)
}


def _group_stream_type(data):
    return {
        k.lower(): tuple(
            _STREAM_TYPE_GROUPER[k](
                map(itemgetter(8, 9, 11), v)  # stream, language, details
            )
        ) for k, v in groupby(
            sorted(data, key=itemgetter(10)),
            key=itemgetter(10)  # type
        )
    }


def _group_input(data):
    for (
        input_,
        format_,
        filepath,
        title,
        encoder,
        creation_time,
        duration,
        bitrate
    ), input_data in groupby(
        data, key=itemgetter(*range(8))
    ):
        struct_time = time.strptime(duration, '%H:%M:%S')
        yield {
            'format': format_,
            'filepath': filepath,
            'title': title,
            'encoder': encoder,
            'creation_time': creation_time and parse(creation_time) or None,
            'duration': timedelta(
                hours=struct_time.tm_hour,
                minutes=struct_time.tm_min,
                seconds=struct_time.tm_sec
            ),
            'bitrate': bitrate,
            'stream': _group_stream_type(input_data)
        }


class AVProbe(object):
    """AVProbe python wrapper."""
    def __init__(self, command_filepath):
        self._command_filepath = command_filepath

        with open(_AVPROBE_OUTPUT_TEMPLATE, 'r', encoding='utf-8') as f:
            self._fsm_parser = TextFSM(f)

    def probe_file(self, filepath):
        output = run([
            self._command_filepath,
            abspath(filepath)
        ], check=True, stdout=PIPE).stdout
        return self.parse_stream(output)

    def parse_stream(self, avprobe_stream):
        data = self._fsm_parser.ParseText(avprobe_stream)
        return tuple(_group_input(data))

    def parse_file(self, avprobe_filepath):
        with open(avprobe_filepath, 'r', encoding='utf-8') as f:
            return self.parse_stream(f.read())
