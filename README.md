# [avprobe](https://libav.org/documentation/avprobe.html) wrapper for python

Wrapper uses [google textfsm](https://github.com/google/textfsm) library to parse avprobe output data (from stdout stream) and convert them to language structure like this:
```python
import datetime

from dateutil.tz import tzutc

from avprobe import AVProbe


avprobe = AVProbe('/usr/bin/avprobe')
avprobe_output = avprobe.parse_file('/path/to/film.avi')

assert avprobe_output == (
    {
        'bitrate': '2042 kb/s',
        'creation_time': datetime.datetime(2017, 12, 7, 12, 33, 26, tzinfo=tzutc()),
        'duration': datetime.timedelta(0, 5750),
        'encoder': 'VirtualDubMod 1.5.10.3',
        'filepath': '/path/to/film.avi',
        'format': 'avi',
        'stream': {
            'audio': (
                {
                    'bitrate': '384 kb/s',
                    'codec': 'ac3',
                    'default': False,
                    'frequency': '48000 Hz',
                    'language': 'eng',
                    'misc': '5.1(side), fltp',
                    'stream': 1
                },
            ),
            'video': (
                {
                    'bitrate': '1648 kb/s',
                    'codec': 'mpeg4 (Advanced Simple Profile), yuv420p',
                    'default': False,
                    'fps': '23.98',
                    'language': 'eng',
                    'misc': '23.98 tbr, 23.98 tbn, 23.98 tbc',
                    'ratio': '720x304 [SAR 1:1 DAR 45:19]',
                    'stream': 0
                },
            )
        },
        'title': 'Film Title'
    },
)
```

